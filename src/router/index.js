import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'
import Chat from '../views/Chat.vue'
import store from '@/store/index';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/chat',
    name: 'Chat',
    component: Chat,
    beforeEnter: (to, from, next) => {
      if(store.getters.isAuthenticated)
        next();
      else{
        next({
          path:'/login',
          query: {
            loggedIn: false,
          }
        });
      }
    }
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue'),
    beforeEnter: (to, from, next) => {
      if(!store.getters.isAuthenticated)
        next();
      else{
        next({
          path:'/',
          query: {
            loggedIn: true,
          }
        });
      }
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
