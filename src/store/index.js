import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      token: localStorage.getItem('token') ? localStorage.getItem('token') : ''
  },
  mutations: {
      setToken(state, payload) {
          localStorage.setItem('token', payload);
          state.token = payload;
      }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    isAuthenticated: state => {
        return state.token.trim().length > 0;
    }
  }
  
})
