module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  chainWebpack: config => {
    config.module.rules.delete('eslint');
  },
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    https: true,
    hotOnly: false,
  }
}